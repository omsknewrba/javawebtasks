package com.app;

import com.app.dao.HouseDaoImpl;
import com.app.dao.UserDaoImpl;
import com.app.dto.HouseDto;
import com.app.dto.UserDto;
import com.app.service.HouseServiceImpl;
import com.app.service.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.core.IsEqual.equalTo;


@ContextConfiguration(classes = AppTestConfiguration.class)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppTestConfiguration.class)
public class MainControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private HouseDaoImpl houseDao;
    @MockBean
    private UserDaoImpl userDao;
    @MockBean
    private UserServiceImpl userService;
    @MockBean
    private HouseServiceImpl houseService;


    @Test
    public void addName() throws Exception {
        String content ="\"Name\"";
        mockMvc.perform(post("/addName")
                .content(content)
                .param("name","Name")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", equalTo("Name")));
    }

    @Test
    public void addSurname() throws Exception {
        String content ="\"Surname\"";
        mockMvc.perform(post("/addSurname")
                .content(content)
                .param("surname","Surname")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.surname", equalTo("Surname")));
    }

    @Test
    public void addPatronymic() throws Exception {
        String content ="\"Patronymic\"";
        mockMvc.perform(post("/addPatronymic")
                .content(content)
                .param("patronymic","Patronymic")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.patronymic", equalTo("Patronymic")));
    }

    @Test
    public void saveUser() {
        UserDto user = new UserDto();
        when(userService.save(user)).thenReturn(true);
    }

    @Test
    public void addHouse() throws Exception {
        String content ="\"15a\"";
        mockMvc.perform(post("/addHouse")
                .content(content)
                .param("number","15a")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.number", equalTo("15a")));
    }

    @Test
    public void viewSelectUser() throws Exception {
        mockMvc.perform(get("/viewSelectUser")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }



    @Test
    public void viewUsers() throws Exception {
        mockMvc.perform(get("/viewUsers")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}