package com.app;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableWebMvc
@ComponentScan("com.app")
@PropertySource("classpath:test.properties")
public class AppTestConfiguration {

    @Autowired
    private Environment environment2;

    @Bean(name = "testDataSource")
    @Profile("test")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment2.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment2.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment2.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment2.getRequiredProperty("jdbc.password"));

        return dataSource;
    }

    @Profile("test")
    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", environment2.getRequiredProperty("hibernate.dialect"));
        properties.put("hibernate.show_sql", environment2.getRequiredProperty("hibernate.show_sql"));
        return properties;
    }

    @Bean(name = "sessionFactoryH2")
    @Profile("test")
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan("com.app.entity");
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }

    @Bean(name = "transactionManagerH2")
    @Profile("test")
    public HibernateTransactionManager transactionManager(SessionFactory s) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(s);
        return txManager;
    }
}
