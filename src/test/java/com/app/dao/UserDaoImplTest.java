package com.app.dao;

import com.app.AppTestConfiguration;
import com.app.entity.Car;
import com.app.entity.House;
import com.app.entity.Person;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@Import(AppTestConfiguration.class)
@ContextConfiguration(classes = AppTestConfiguration.class)
@Transactional
@WebAppConfiguration
@ActiveProfiles("test")
public class UserDaoImplTest {

    @Autowired
    UserDao userDao;

    @Test
    public void save() {
        List<House> houses = new ArrayList<>();
        List<Car> cars = new ArrayList<>();
        Person user = new Person("a","a","a",houses,cars);
        Assert.assertTrue(userDao.save(user));
    }

    @Test
    public void findAll() {
        List<Person> persons;
        persons = userDao.findAll();
        assertNotNull(persons);
    }

    @Test
    public void findById() {
        Optional<Person> byId = Optional.ofNullable(userDao.findById(4));
        assertNotNull(byId);
    }

}