package com.app.dao;

import com.app.AppTestConfiguration;
import com.app.entity.Car;
import com.app.entity.House;
import com.app.entity.Person;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@Import(AppTestConfiguration.class)
@ContextConfiguration(classes = AppTestConfiguration.class)
@Transactional
@WebAppConfiguration
@ActiveProfiles("test")
public class HouseDaoImplTest {

    @Autowired
    private HouseDaoImpl houseDao;

    @Test
    public void save() {
        List<House> userHouse = new ArrayList<>();
        List<Car> userCar = new ArrayList<>();
        Person user = new Person("a","a","a",userHouse,userCar);
        House house = new House("418",user);
        Assert.assertNotNull(houseDao.save(house));
    }

    @Test
    public void findById() {
        Optional<House> byId = Optional.ofNullable(houseDao.findById(419));
        Assert.assertTrue(byId != null);
    }

    @Test
    public void findAll() {
        List<House> houses = new ArrayList<House>();
        houses = houseDao.findAll();
        assertNotNull(houses);
    }
}