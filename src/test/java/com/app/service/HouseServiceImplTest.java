package com.app.service;

import com.app.dao.HouseDao;
import com.app.dto.HouseDto;
import com.app.entity.House;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class HouseServiceImplTest {

    @InjectMocks
    private HouseServiceImpl houseService;

    @Mock
    private HouseDao houseDao;

    @Test
    public void save() {
        HouseDto house = new HouseDto();
        assertNotNull(houseService.save(house));
    }

    @Test
    public void findAll() {
        List<House> houses = houseService.findAll();
        assertNotNull(houses);
    }
    @Test
    public void findById(){
        int id = 98;
        houseService.findById(id);
        verify(houseDao, times(1)).findById(id);
    }
}