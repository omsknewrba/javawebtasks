package com.app.service;

import com.app.dao.UserDao;
import com.app.dto.UserDto;
import com.app.entity.Person;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserDao userDao;

    @Test
    public void findAll() {
        List<UserDto> persons;
        persons = userService.findAll();
        assertNotNull(persons);
    }

    @Test
    public void save() {
        UserDto user = new UserDto();
        Assert.assertTrue(userService.save(user));
    }

    @Test
    public void findById(){
        int id = 98;
        userService.findById(id);
        verify(userDao, times(1)).findById(id);
    }
}