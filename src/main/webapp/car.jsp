<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<head>
	<title>Автомобиль</title>
</head>
<body>
	<form name="model" method="post" action="/addCar">
		<h1>Введите Модель</h1>
		<fieldset>
		    <p><strong>Модель:</strong></p>
			<input type="text" name="input" required="">
    		 <p><input type="submit" value="Отправить">
    		 	<input type="reset" value="Очистить"></p>
    	</fieldset>
	</form>
    <script type="text/javascript" src='<c:url value="/resources/form.js"/>'></script>
</body>
</html>