(function(){
    $(document).ready(function(){

        $.ajax({
        url:'http://localhost:8080/viewUsers',
        method:'GET',
        dataType: 'json',
        contentType:'application/json',
        }).done(function(data){
          let tbody = document.getElementById('userdata').tBodies[0];
          data.forEach(function(user){
                let row = tbody.insertRow();
                for (key in user) {
                    let cell = row.insertCell();
                   if(user[key]===user['houses']){
                       for (value in user[key]){
                           if(value == user[key].length -1){
                           cell.append(user[key][value].number);
                           }
                           else cell.append(user[key][value].number + ", ");
                       }
                   }
                    else if(user[key]===user['cars']){
                        for (value in user[key]){
                           if(value == user[key].length -1){
                           cell.append(user[key][value].model);
                           }
                           else cell.append(user[key][value].model + ", ");
                        }
                    }
                    else cell.innerHTML = user[key];
                }
          });
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);
        });
    });
})();