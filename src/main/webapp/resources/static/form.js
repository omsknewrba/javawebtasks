(function(){
    $(document).ready(function () {
        $('form').submit(function(event){
            event.preventDefault();
            let $data =$('input[name="input"]').val();
            let $formName = $(this).attr('name');
            let jsonData = {[$formName] : $data};
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                dataType:'json',
                contentType:'application/json; charset=utf-8',
                data:JSON.stringify(jsonData),
            }).done(function(data){
            if($formName === 'number'){
            document.location.assign('http://localhost:8080/selectHouseUser.jsp');
            }else if($formName === 'model'){
            document.location.assign('http://localhost:8080/selectCarUser.jsp');
            }else if($formName === 'name'){
            document.location.assign('http://localhost:8080/surname.jsp');
            } else if($formName === 'surname'){
            document.location.assign('http://localhost:8080/patronymic.jsp');
            } else document.location.assign('http://localhost:8080/index.jsp');
            }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText);
            });
        });
    });
})();

