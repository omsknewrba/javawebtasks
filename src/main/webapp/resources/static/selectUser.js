(function(){
    $(document).ready(function () {
    $.ajax({
        url:'http://localhost:8080/viewSelectUser',
        method:'GET',
        dataType: 'json',
        contentType:'application/json; charset=utf-8',
        }).done(function(data){
        console.log(data);
          data.forEach(function(item){
              $('#selectUser').append($("<option></option>").attr("value", item.id).html(item.name + ' ' + item.surname + ' ' + item.patronymic));
          });
        }).fail(function(jqXHR, textStatus, errorThrown){
        console.log(jqXHR.responseText);
        });

   $('form').submit(function(event){
       event.preventDefault();
       let user = $('#selectUser').val();
       let $formName = $(this).attr('name');
       $.ajax({
               type: $(this).attr('method'),
               url: $(this).attr('action'),
               dataType:'json',
               contentType:'application/json; charset=utf-8',
               //data:JSON.stringify(user),
               data:JSON.stringify({id:user}),
               }).done(function(data){
               document.location.assign('http://localhost:8080/index.jsp');
               }).fail(function(jqXHR, textStatus, errorThrown){
               console.log(jqXHR.responseText);
               });
       });
   });
})();