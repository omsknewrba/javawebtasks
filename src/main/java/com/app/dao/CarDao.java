package com.app.dao;

import com.app.entity.Car;
import java.util.List;
import java.util.Map;

public interface CarDao {

    boolean save(Car car);

    Car findById(int id);

    List<Car> findAll();
}
