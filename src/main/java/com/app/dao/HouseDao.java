package com.app.dao;

import com.app.entity.House;
import java.util.List;
import java.util.Map;

public interface HouseDao {

    boolean save(House house);

    House findById(int id);

    List<House> findAll();
}
