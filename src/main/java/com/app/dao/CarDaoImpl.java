package com.app.dao;

import com.app.entity.Car;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CarDaoImpl implements CarDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public boolean save(Car car) {
        Session session = sessionFactory.getCurrentSession();
        try{
            session.merge(car);
            return true;
        } catch(Exception e) {
            return false;
        }

    }

    @Override
    public Car findById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Car.class,id);
    }
    @Override
    public List<Car> findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Car", Car.class).list();
    }
}
