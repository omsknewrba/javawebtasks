package com.app.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.app.entity.Person;

import java.util.List;
import java.util.Map;

/**
 * this class used to interaction with database
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * this method get object "user", obtains the current session and save object in db
     */
    @Override
    public boolean save(Person user) {
        Session session = sessionFactory.getCurrentSession();
        try{
            session.persist(user);
            return true;
        } catch(Exception e) {
            return false;
        }

    }
    /**
     * this method obtains the current session and executes the HQL query to get all users in db
     */
    @Override
    public List<Person> findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from Person", Person.class).list();
    }
    /**
     * this method obtains the current session and search object for id
     */
    @Override
    public Person findById(int id) {
        Session session = sessionFactory.getCurrentSession();
         return session.get(Person.class,id);
    }
}
