package com.app.dao;

import com.app.entity.Person;
import java.util.List;
import java.util.Map;

public interface UserDao {

    boolean save(Person user);

    List<Person> findAll();

    Person findById(int id);
}
