package com.app.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.app.entity.House;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this class implements all the functionality required for interact with database
 */
@Repository
public class HouseDaoImpl implements HouseDao {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * this method get object and save in database
     * @param house - object to be saved in db
     * @return - map with id of saved house and id of person mapped with him
     */
    @Override
    public boolean save(House house) {

        Session session = sessionFactory.getCurrentSession();
        try{
            session.merge(house);
            return true;
        } catch(Exception e) {
            return false;
        }

    }

    /**
     * this method obtains the current session and search object for id
     */
    @Override
    public House findById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(House.class,id);
    }

    /**
     * this method obtains the current session and executes the HQL query to get all houses in db
     */
    @Override
    public List<House> findAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("from House", House.class).list();
    }
}

