package com.app.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * this class used as an entity
 */
@Entity
@Table(name = "house")

public class House implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "number", nullable = false)
    private String number;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private Person person;

    public House() {
    }

    public House(String number) {
        this.number = number;
    }

    public House(String number,Person person) {
        this.number = number;
        this.person = person;
    }
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return id == house.id &&
                Objects.equals(number, house.number) &&
                Objects.equals(person, house.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, person);
    }

    @Override
    public String toString() {
        return "House{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", person=" + person +
                '}';
    }
}
