package com.app.service;
import com.app.dto.CarDto;
import com.app.entity.Car;

import java.util.List;
import java.util.Map;

public interface CarService {

    boolean save(CarDto carDto);

    List<Car> findAll();

    Car findById(int id);
}
