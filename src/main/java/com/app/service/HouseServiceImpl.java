package com.app.service;

import com.app.dto.HouseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.entity.House;
import com.app.dao.HouseDao;

import java.util.List;
import java.util.Map;

import static org.springframework.beans.BeanUtils.copyProperties;

/**
 * this class used as a service for interacting with dao layer
 */
@Service
public class HouseServiceImpl implements HouseService {

    @Autowired
    HouseDao houseDao;

    /**
     * this method get dto, convert dto to entity and call dao method to save object
     */
    @Override
    @Transactional
    public boolean save(HouseDto houseDto) {
        House house = new House();
        copyProperties(houseDto,house);
        return houseDao.save(house);
    }
    /**
     * this method call dao method to get all users
     */
    @Override
    @Transactional
    public List<House> findAll() {
        return houseDao.findAll();
    }
    /**
     * this method call dao method to get object by id
     */
    @Override
    @Transactional
    public House findById(int id) {return houseDao.findById(id);}
}
