package com.app.service;

import com.app.dto.UserDto;
import com.app.entity.Person;
import java.util.List;
import java.util.Map;

public interface UserService {

    boolean save(UserDto userDto);

    List<UserDto> findAll();

    Person findById(int id);
}
