package com.app.service;

import com.app.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.entity.Person;
import com.app.dao.UserDao;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.springframework.beans.BeanUtils.copyProperties;

/**
 * this class used as a service for interacting with dao layer
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UserDao userDao;

    /**
     * this method call dao method to get all users
     */
    @Override
    @Transactional
    public List<UserDto> findAll() {
        List<UserDto> userDtoList = new ArrayList<>();
        List<Person> userList = userDao.findAll();
        for(Person i : userList){
            UserDto userDto = new UserDto();
            copyProperties( i,userDto);
            userDtoList.add(userDto);
        }
        return userDtoList;
    }

    /**
     * this method get dto, convert dto to entity and call dao method to save object
     */
    @Override
    @Transactional
    public boolean save(UserDto userDto) {
        Person user = new Person();
        copyProperties(userDto,user);
        userDao.save(user);
        return true;
    }
    /**
     * this method call dao method to get object by id
     */
    @Override
    @Transactional
    public Person findById(int id) {return userDao.findById(id);}
}
