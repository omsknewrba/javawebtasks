package com.app.service;

import com.app.dto.HouseDto;
import com.app.entity.House;
import java.util.List;
import java.util.Map;

public interface HouseService {
    boolean save(HouseDto houseDto);

    List<House> findAll();

    House findById(int id);
}
