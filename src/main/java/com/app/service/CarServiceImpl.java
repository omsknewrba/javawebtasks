package com.app.service;

import com.app.dao.CarDao;
import com.app.dto.CarDto;
import com.app.entity.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static org.springframework.beans.BeanUtils.copyProperties;

@Service
public class CarServiceImpl implements CarService{

    @Autowired
    CarDao carDao;

    /**
     * this method get dto, convert dto to entity and call dao method to save object
     */
    @Override
    @Transactional
    public boolean save(CarDto carDto) {
        Car car = new Car();
        copyProperties(carDto,car);
        return carDao.save(car);
    }
    /**
     * this method call dao method to get all cars
     */
    @Override
    @Transactional
    public List<Car> findAll() {
        return carDao.findAll();
    }
    /**
     * this method call dao method to get object by id
     */
    @Override
    @Transactional
    public Car findById(int id) {
        return carDao.findById(id);
    }
}
