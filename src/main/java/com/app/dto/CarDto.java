package com.app.dto;

import com.app.entity.Person;
import org.springframework.stereotype.Component;

@Component
public class CarDto {
    private int id;
    private String model;
    private Person person;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
