package com.app.dto;

import com.app.entity.Car;
import com.app.entity.House;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * this class used as a DTO
 */
@Component
public class UserDto implements Serializable {
    private int id;
    private String name;
    private String surname;
    private String patronymic;
    private List<House> houses;
    private List<Car> cars;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public List<House> getHouses() {
        return houses;
    }

    public void setHouses(List<House> houses) {
        this.houses = houses;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }
}
