package com.app.controller;

import com.app.dto.CarDto;
import com.app.dto.HouseDto;
import com.app.dto.UserDto;
import com.app.entity.Car;
import com.app.entity.House;
import com.app.entity.Person;
import com.app.service.CarService;
import com.app.service.HouseService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.app.service.UserService;
import java.util.List;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * this class get JSON user data and sends dto to service for save
 */
@RestController
public class MainController {

    @Autowired
    private UserDto userDto;

    @Autowired
    private HouseDto houseDto;

    @Autowired
    private CarDto carDto;

    @Autowired
    public UserService userService;

    @Autowired
    public HouseService houseService;

    @Autowired
    public CarService carService;

    /**
     * method get inputted parameter "name" and save in dto
     */
    @PostMapping(value = "/addName", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Person addName(@RequestBody String name) throws JsonProcessingException {
        ObjectMapper mapper=new ObjectMapper();
        Person personName = mapper.readValue(name,Person.class);
        userDto.setName(personName.getName());
        return personName;
    }

    /**
     * method get inputted parameter "surname" and save in dto
     */
    @PostMapping(value = "/addSurname", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Person addSurname(@RequestBody String surname) throws JsonProcessingException {
        ObjectMapper mapper=new ObjectMapper();
        Person personSurname = mapper.readValue(surname,Person.class);
        userDto.setSurname(personSurname.getSurname());
        return personSurname;
    }

    /**
     * method get inputted parameter "patronymic" and save in dto and call saveUser() method
     */
    @PostMapping(value ="/addPatronymic", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Person addPatronymic(@RequestBody String patronymic) throws Exception {
        ObjectMapper mapper=new ObjectMapper();
        Person personPatronymic= mapper.readValue(patronymic,Person.class);
        userDto.setPatronymic(personPatronymic.getPatronymic());
        saveUser(userDto);
        return personPatronymic;
    }

    /**
     * this method get dto and call servic re method to save object
     */
    @GetMapping("/saveUser")
    public void saveUser(UserDto userDto) {
        if (!isEmpty(userDto.getName())&&
            !isEmpty(userDto.getSurname())&&
            !isEmpty(userDto.getPatronymic())){

         userService.save(userDto);
        }
    }

    /**
     * method get inputted parameter "number" and save in dto
     */
    @PostMapping(value = "/addHouse", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public House addHouse(@RequestBody String number) throws JsonProcessingException{
        ObjectMapper mapper=new ObjectMapper();
        House houseNumber = mapper.readValue(number,House.class);
        houseDto.setNumber(houseNumber.getNumber());

        return houseNumber;
    }

    /**
     * this method get map, call service to get dto list with all users and put list in map
     */
    @GetMapping(value = "/viewSelectUser", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<UserDto> viewSelectUser(){
        return userService.findAll();
    }

    /**
     * this method get user id, call service to find user by id and set this user for dto
     */
    @PostMapping("/addHouseUser")
    public boolean addHouseUser(@RequestBody String userId) throws Exception {
        ObjectMapper mapper=new ObjectMapper();
        Person houseUser = mapper.readValue(userId,Person.class);
        houseDto.setPerson(userService.findById(houseUser.getId()));
        saveHouse(houseDto);
        return true;
    }

    /**
     * this method call service method to save dto object
     */
    @GetMapping(value = "/saveHouse")
    public boolean saveHouse(HouseDto houseDto) {
       return houseService.save(houseDto);
    }

    @PostMapping(value = "/addCar", produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Car addCar(@RequestBody String model) throws JsonProcessingException {
        ObjectMapper mapper=new ObjectMapper();
        Car car = mapper.readValue(model, Car.class);
        carDto.setModel(car.getModel());
        return car;
    }

    /**
     * this method get user id, call service to find user by id and set this user for dto
     */
    @PostMapping("/addCarUser")
    public boolean addCarUser(@RequestBody String userId) throws Exception {
        ObjectMapper mapper=new ObjectMapper();
        Person carUser = mapper.readValue(userId,Person.class);
        carDto.setPerson(userService.findById(carUser.getId()));
        saveCar(carDto);
        return true;
    }

    /**
     * this method call service method to save dto object
     */
    @GetMapping("/saveCar")
    public boolean saveCar(CarDto CarDto) {
        return carService.save(carDto);
    }

    /**
     * this method call service method to get all users
     * and send model to viewing on the next page
     */
    @GetMapping(value = "/viewUsers", produces = "application/json",consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<UserDto> viewUsers(){
        return userService.findAll();
    }

}
